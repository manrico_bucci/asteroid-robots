#!/usr/bin/python
"""Robots moving on asteroids"""
import argparse
import json


def robot_exception(message):
    """basic exception message"""
    return json.dumps({"type": "error", "error_message": message})


def parse_instruction(instruction_json):
    """parse a json instruction and returns a dictionary or an exception"""
    try:
        return json.loads(instruction_json)
    except ValueError:
        return {"type": "error", "error_message": "Not a valid json instruction"}


class Asteroid(object):
    """Asteroid class"""
    x = 0
    y = 0
    robots = []

    def __init__(self, size):
        self.x = size.get('x', 0)
        self.y = size.get('y', 0)
        if not self.x or not self.y:
            return robot_exception('Asteroid initialized with non positive size.')

    def get_size(self):
        """asteroid size as dictionary"""
        return {'x': self.x, 'y': self.y}


class Robot(object):
    """Robot class"""
    x = 0
    y = 0
    bearing = 0
    CARDINALS = ['north', 'east', 'south', 'west']

    def __init__(self, position={'x': 0, 'y': 0}, bearing='north'):
        self.x = position.get('x', 0)
        self.y = position.get('y', 0)
        self.bearing = self.CARDINALS.index(bearing)

    def move(self, movement, asteroid):
        """move the robot on the asteroid"""
        if movement == 'turn-right':
            self.bearing += 1
        if movement == 'turn-left':
            self.bearing -= 1
        elif movement == 'move-forward':
            if self.get_bearing() == "north":
                self.y += 1
            elif self.get_bearing() == 'east':
                self.x += 1
            elif self.get_bearing() == 'south':
                self.y -= 1
            elif self.get_bearing() == 'west':
                self.x -= 1
        is_position_correct = self.check_position(asteroid)
        if is_position_correct:
            return json.dumps({"type": "robot",
                               "position": {"x": self.x, "y": self.y},
                               "bearing": self.get_bearing()})
        else:
            return robot_exception("The robot went out of the asteroid. Moved back to the edge.")

    def get_position(self):
        """
        :return:position returned as dictionary
        """
        return {'x': self.x, 'y': self.y}

    def get_bearing(self):
        """
        :return: bearing returned as string
        """
        self.bearing %= 4
        return self.CARDINALS[self.bearing]

    def get_info(self):
        """
        :return: dictionary with all robot's info
        """
        return {"type": "robot", "position": self.get_position(), "bearing": self.get_bearing()}

    def check_position(self, asteroid):
        """
        check if the robots position is in the asteroid
        :param asteroid: the asteroid object
        :return: true if the robot is in the asteroid, false if it's outside
        """
        if self.x > asteroid.x:
            self.x = asteroid.x  # move back the robot on the asteroid edge
            return False
        if self.x < 0:
            self.x = 0
            return False
        if self.y > asteroid.y:
            self.y = asteroid.y
            return False
        if self.y < 0:
            self.y = 0
            return False
        return True


def main():
    """
    parse an instruction file list passed as parameter and execute the instructions accordingly
    prints the final status of the defined robots
    """
    parser = argparse.ArgumentParser(description='Asteroid Robots')
    parser.add_argument('instructions', nargs='+', help='a file containing a list of instructions')
    args = parser.parse_args()
    file_list = args.instructions

    robot_list = []

    for instr_file in file_list:
        with open(instr_file) as instructions:
            for instruction in instructions:
                instruction_dict = parse_instruction(instruction)
                instruction_type = instruction_dict.get('type', None)
                if not instruction_type:
                    print robot_exception('type field not specified in instruction')
                if instruction_type == 'asteroid':
                    size = instruction_dict.get('size', None)
                    if not size:
                        print robot_exception('size field not specified for type asteroid in instruction')
                        return
                    asteroid = Asteroid(size)
                elif instruction_type == 'new-robot':

                    position = instruction_dict.get('position', None)
                    if not position:
                        print robot_exception('position field not specified for type new-robot in instruction')
                        return

                    bearing = instruction_dict.get('bearing', None)
                    if not bearing:
                        print robot_exception('bearing field not specified for type new-robot in instruction')

                    robot = Robot(position, bearing)
                    robot_list.append(robot)
                elif instruction_type == 'move':
                    movement = instruction_dict.get('movement', None)
                    if not movement:
                        print robot_exception('movement field not specified for type move in instruction')
                        return

                    robot.move(movement, asteroid)
        for robot in robot_list:
            print json.dumps(robot.get_info())


if __name__ == '__main__':
    main()
