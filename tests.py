import unittest
import json
from robots import Robot, Asteroid


class TestRobots(unittest.TestCase):

    def setUp(self):
        position = {"x": 0, "y": 0}
        self.robot = Robot(position)
        size = {"x": 5, "y": 5}
        self.asteroid = Asteroid(size)

    def test_create_robot(self):
        robot = Robot()
        self.assertEqual(robot.x, 0)
        self.assertEqual(robot.y, 0)
        self.assertEqual(robot.get_position(), {'x': 0, 'y': 0})

    def test_robot_turn_right(self):
        self.assertEqual(self.robot.get_bearing(), 'north')
        movement = 'turn-right'
        output = self.robot.move(movement, self.asteroid)
        self.assertEqual(json.loads(output)['bearing'], 'east')
        self.assertEqual(self.robot.get_bearing(), 'east')
        # turning again right
        output = self.robot.move(movement, self.asteroid)
        self.assertEqual(json.loads(output)['bearing'], 'south')
        self.assertEqual(self.robot.get_bearing(), 'south')
        # turning again right
        output = self.robot.move(movement, self.asteroid)
        self.assertEqual(json.loads(output)['bearing'], 'west')
        self.assertEqual(self.robot.get_bearing(), 'west')
        # turning again right
        output = self.robot.move(movement, self.asteroid)
        self.assertEqual(json.loads(output)['bearing'], 'north')
        self.assertEqual(self.robot.get_bearing(), 'north')

    def test_robot_turn_left(self):
        self.assertEqual(self.robot.get_bearing(), 'north')
        movement = 'turn-left'
        output = self.robot.move(movement, self.asteroid)
        self.assertEqual(json.loads(output)['bearing'], 'west')
        self.assertEqual(self.robot.get_bearing(), 'west')
        # turning again right
        output = self.robot.move(movement, self.asteroid)
        self.assertEqual(json.loads(output)['bearing'], 'south')
        self.assertEqual(self.robot.get_bearing(), 'south')
        # turning again right
        output = self.robot.move(movement, self.asteroid)
        self.assertEqual(json.loads(output)['bearing'], 'east')
        self.assertEqual(self.robot.get_bearing(), 'east')
        # turning again right
        output = self.robot.move(movement, self.asteroid)
        self.assertEqual(json.loads(output)['bearing'], 'north')
        self.assertEqual(self.robot.get_bearing(), 'north')

    def test_robot_move_forward(self):
        # going north
        output = self.robot.move('move-forward', self.asteroid)
        self.assertEqual(json.loads(output)['position']['x'], 0)
        self.assertEqual(json.loads(output)['position']['y'], 1)
        self.assertEqual(self.robot.get_position(), {'x': 0, 'y': 1})
        # going east
        self.robot.move('turn-right', self.asteroid)
        output = self.robot.move('move-forward', self.asteroid)
        self.assertEqual(json.loads(output)['position']['x'], 1)
        self.assertEqual(json.loads(output)['position']['y'], 1)
        self.assertEqual(self.robot.get_position(), {'x': 1, 'y': 1})
        output = self.robot.move('move-forward', self.asteroid)
        self.assertEqual(json.loads(output)['position']['x'], 2)
        self.assertEqual(json.loads(output)['position']['y'], 1)
        self.assertEqual(self.robot.get_position(), {'x': 2, 'y': 1})
        # going south
        self.robot.move('turn-right', self.asteroid)
        output = self.robot.move('move-forward', self.asteroid)
        self.assertEqual(json.loads(output)['position']['x'], 2)
        self.assertEqual(json.loads(output)['position']['y'], 0)
        self.assertEqual(self.robot.get_position(), {'x': 2, 'y': 0})
        # going west
        self.robot.move('turn-right', self.asteroid)
        output = self.robot.move('move-forward', self.asteroid)
        self.assertEqual(json.loads(output)['position']['x'], 1)
        self.assertEqual(json.loads(output)['position']['y'], 0)
        self.assertEqual(self.robot.get_position(), {'x': 1, 'y': 0})
        output = self.robot.move('move-forward', self.asteroid)
        self.assertEqual(json.loads(output)['position']['x'], 0)
        self.assertEqual(json.loads(output)['position']['y'], 0)
        self.assertEqual(self.robot.get_position(), {'x': 0, 'y': 0})

    def test_robot_move_out_asteroid(self):
        # going off the asteroid
        self.robot.move('turn-left', self.asteroid)
        output = self.robot.move('move-forward', self.asteroid)
        self.assertEqual(json.loads(output)['type'], 'error')
        self.assertEqual(json.loads(output)['error_message'],
                         'The robot went out of the asteroid. Moved back to the edge.')

    def test_robot_check_position(self):
        self.assertTrue(self.robot.check_position(self.asteroid))
        # out of asteroid on the right
        self.robot.x = self.asteroid.x + 2
        self.assertFalse(self.robot.check_position(self.asteroid))
        self.assertEqual(self.robot.x, self.asteroid.x)
        self.assertTrue(self.robot.check_position(self.asteroid))
        # out of asteroid on the left
        self.robot.x = -2
        self.assertFalse(self.robot.check_position(self.asteroid))
        self.assertEqual(self.robot.x, 0)
        self.assertTrue(self.robot.check_position(self.asteroid))
        # out of asteroid on the up
        self.robot.y = self.asteroid.y + 1
        self.assertFalse(self.robot.check_position(self.asteroid))
        self.assertEqual(self.robot.y, self.asteroid.y)
        self.assertTrue(self.robot.check_position(self.asteroid))
        # out of asteroid on the down
        self.robot.y = -1
        self.assertFalse(self.robot.check_position(self.asteroid))
        self.assertEqual(self.robot.y, 0)
        self.assertTrue(self.robot.check_position(self.asteroid))

    # def test_instructions
if __name__ == '__main__':
    unittest.main()
